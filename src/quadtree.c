/* -*- c-basic-offset: 3 -*- 
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin 
 * F-14050 Caen Cedex 
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author SISSOKO Djadjiri <djadjiri.sissoko@ecole.ensicaen.fr> 
 * @version     0.0.1 - Sep 20, 2010
 * 
 * @todo the list of improvements suggested for the file.
 * @bug the list of known bugs.
 */

/**
 * @file image_util.c
 *  
 * Description of the program objectives.
 * All necessary references.
 */

#include <stdio.h>
#include <stdlib.h>
#include <image.h> 
#include <image_util.h>
#include <quadtree.h>

/**
 * Create a quadtree with only one node .
 *
 * @return quadtree.
 */
extern pQuadtree create_quadtree()
{
  
  pQuadtree quadtree;

  quadtree.noeud = NULL;
  
  return quadtree;
}

/**
 * Initialize the sons of a quadtree.
 *
 * @param quadtree pointer.
 */
extern void quadtree_subdivide(pQuadtree quadtree)
{
   quadtree.noeud->sons[0]=create_quadtree();
   quadtree.noeud->sons[1]=create_quadtree();
   quadtree.noeud->sons[2]=create_quadtree();
   quadtree.noeud->sons[3]=create_quadtree();

   quadtree.noeud->M0=0;

   quadtree.noeud->M1[0]=0.;
   quadtree.noeud->M1[1]=0.;
   quadtree.noeud->M1[2]=0.;
   
   quadtree.noeud->M2[0]=0.;
   quadtree.noeud->M2[1]=0.;
   quadtree.noeud->M2[2]=0.;
}


/**
 * Delete the whole quadtree using ascending recursivity.
 *
 * @param quadtree pointer.
 */
extern void delete_quadtree(pQuadtree quadtree)
{
   if (quadtree.noeud != NULL)
      {
	 delete_quadtree(quadtree.noeud->sons[0]);
	 delete_quadtree(quadtree.noeud->sons[1]);
	 delete_quadtree(quadtree.noeud->sons[2]);
	 delete_quadtree(quadtree.noeud->sons[3]);

	 free(quadtree.noeud);
	 quadtree.noeud=NULL;
      }
	 
}
