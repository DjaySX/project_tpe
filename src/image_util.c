/* -*- c-basic-offset: 3 -*- 
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin 
 * F-14050 Caen Cedex 
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author SISSOKO Djadjiri <djadjiri.sissoko@ecole.ensicaen.fr> 
 * @version     0.0.1 - Sep 20, 2010
 * 
 * @todo the list of improvements suggested for the file.
 * @bug the list of known bugs.
 */

/**
 * @file image_util.c
 *  
 * Description of the program objectives.
 * All necessary references.
 */

#include <stdio.h>
#include <stdlib.h>
#include <image.h> 
#include <image_util.h>

/**
 * A complete description of the function.
 *
 * @param par1 description of the paramter par1.
 * @return description of the result.
 */
extern void draw_square(image img,int xmin,int ymin,int xmax,int ymax,int* color)
{
   Point coord;
   int tmpx;
   int tmpy;
   
   COORDX(coord)=xmin;
   COORDY(coord)=ymin;
   image_move_to(img,&coord);
   tmpx=xmin;
   tmpy=ymin;
   
   do
      {
	 image_ecrire_pixel(img,color);
	 tmpx++;
      }while(image_pixel_droite(img) && tmpx<=xmax);

   do
      {
	 image_ecrire_pixel(img,color);
	 tmpy++;
      }while(image_pixel_dessous(img) && tmpy<=ymax);

   do
      {
	 image_ecrire_pixel(img,color);
	 tmpx--;
      }while(image_pixel_gauche(img) && tmpx>=xmin);

   do
      {
	 image_ecrire_pixel(img,color);
	 tmpy--;
      }while(image_pixel_dessus(img) && tmpy>=ymin);
   
}

/**
 * A complete description of the function.
 *
 * @param par1 description of the paramter par1.
 * @return description of the result.
 */
extern void give_moments(image img,int xmin,int ymin,int xmax,int ymax,int* m0,double* m1,double* m2)
{
   int tmpy,tmpx;
   Point coord;
   int* tabtmp;
   int Dim = image_give_dim(img);

   *m0=(ymax-ymin)*(xmax-xmin);
   
   COORDX(coord)=xmin;
   COORDY(coord)=ymin;
   image_move_to(img,&coord);
   tmpx = xmin;
   tmpy = ymin;
   
   
   if(Dim==1)
      {
	 *m1=0.;
	 *m2=0.;
      }
   else
      {
	 m1[0]=0.;
	 m1[1]=0.;
	 m1[2]=0.;
	 m2[0]=0.;
	 m2[1]=0.;
	 m2[2]=0.;
      }
   
   do
      {
	 if(tmpx==xmin)
	    {
	       do
		  {
		     tabtmp=image_lire_pixel(img);
		     if(Dim==1)
			{
			   *m1+=*tabtmp;
			   *m2+=*tabtmp*(*tabtmp);
			}
		     else
			{
			   m1[0]+=tabtmp[0];
			   m1[1]+=tabtmp[1];
			   m1[2]+=tabtmp[2];
			   m2[0]+=tabtmp[0]*tabtmp[0];
			   m2[1]+=tabtmp[1]*tabtmp[1];
			   m2[2]+=tabtmp[2]*tabtmp[2];
			}
		     tmpx++;
		  }while(tmpx<xmax && image_pixel_droite(img));
	    }
	 else
	    {
	       do
		  {
		     tabtmp=image_lire_pixel(img);
		     if(Dim==1)
			{
			   *m1+=*tabtmp;
			   *m2+=*tabtmp*(*tabtmp);
			}
		     else
			{
			   m1[0]+=tabtmp[0];
			   m1[1]+=tabtmp[1];
			   m1[2]+=tabtmp[2];
			   m2[0]+=tabtmp[0]*tabtmp[0];
			   m2[1]+=tabtmp[1]*tabtmp[1];
			   m2[2]+=tabtmp[2]*tabtmp[2];
			}
		     tmpx--;
		  }while(tmpx>xmin && image_pixel_gauche(img));
	       
	    }
	 tabtmp=image_lire_pixel(img);
	 if(Dim==1)
	    {
	       *m1+=*tabtmp;
	       *m2+=*tabtmp*(*tabtmp);
	    }
	 else
	    {
	       m1[0]+=tabtmp[0];
	       m1[1]+=tabtmp[1];
	       m1[2]+=tabtmp[2];
	       m2[0]+=tabtmp[0]*tabtmp[0];
	       m2[1]+=tabtmp[1]*tabtmp[1];
	       m2[2]+=tabtmp[2]*tabtmp[2];
	    }
	 printf("%d %d\n",tmpx, tmpy);
	 printf("m0 = %d\n m1 = %f/%f/%f\n m2 = %f/%f/%f\n",*m0,m1[0],m1[1],m1[2],m2[0],m2[1],m2[2]);
	 tmpy++;
      }while(image_pixel_dessous(img) && tmpy<=ymax);
}

