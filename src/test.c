/* -*- c-basic-offset: 3 -*- 
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin 
 * F-14050 Caen Cedex 
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author SISSOKO Djadjiri <djadjiri.sissoko@ecole.ensicaen.fr> 
 * @version     0.0.1 - Sep 20, 2010
 * 
 * @todo the list of improvements suggested for the file.
 * @bug the list of known bugs.
 */

/**
 * @file test.c
 *  
 * Description of the program objectives.
 * All necessary references.
 */

#include <stdio.h> 
#include <image.h>
#include <image_util.h>

int main() {
   image img=FAIRE_image();
   int color[3]={255,0,0};
   int col[3];
   int bleu []={255,255,255};
   int i;
   int m0;
   double m1[3];
   double m2[3];

   image_initialize(img,3,500,500);
   image_debut(img); /* On se positionne au début*/
   
   /* Test draw_square */
   draw_square(img,0,0,50,50,bleu);

  /* Test give_moments */
   give_moments(img, 0,0,50,50,&m0,m1,m2);

   /*
   printf("m0 = %d\n m1 = %f/%f/%f\n m2 = %f/%f/%f\n",m0,m1[0],m1[1],m1[2],m2[0],m2[1],m2[2]);
   */
   image_sauvegarder(img,"test_square.ppm");
   DEFAIRE_image(img);
   
   return 0;
}
