CC=gcc
CFLAGS=-Wall -Wextra -ansi -pedantic -I./include -I./MODULE_IMAGE -g
LIBS=-L./MODULE_IMAGE
OBJ=test.o image_util.o quadtree.o
OUTPUT=test

all: $(OUTPUT)

$(OUTPUT): $(OBJ)
	$(CC) $(LIBS) $(OBJ) -g -o $@ -limage64

test.o: src/test.c include/image_util.h
	$(CC) $(LIBS) $(CFLAGS) -c $<

image_util.o: src/image_util.c include/image_util.h
	$(CC) $(LIBS) $(CFLAGS) -c $<

quadtree.o: src/quadtree.c include/quadtree.h
	$(CC) $(LIBS) $(CFLAGS) -c $<
