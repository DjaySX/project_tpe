/* -*- c-basic-offset: 3 -*- 
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin 
 * F-14050 Caen Cedex 
 * 
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author SISSOKO Djadjiri <djadjiri.sissoko@ecole.ensicaen.fr> 
 * @version     0.0.1 - Sep 20, 2010
 * 
 * @todo the list of improvements suggested for the file.
 * @bug the list of known bugs.
 */

#ifndef __IMAGEUTIL_H
#define __IMAGEUTIL_H


/**
 * @file image_util.h
 *  
 * Description of the program objectives.
 * All necessary references.
 */

#include <stdio.h> 

extern void draw_square(image img,int xmin,int ymin,int xmax,int ymax,int* color);
extern void give_moments(image img,int xmin,int ymin,int xmax,int ymax,int* m0,double* m1,double* m2);
#endif
