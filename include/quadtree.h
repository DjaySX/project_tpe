/* -*- c-basic-offset: 3 -*- 
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin 
 * F-14050 Caen Cedex 
 * 
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author SISSOKO Djadjiri <djadjiri.sissoko@ecole.ensicaen.fr> 
 * @version     0.0.1 - Sep 20, 2010
 * 
 * @todo the list of improvements suggested for the file.
 * @bug the list of known bugs.
 */

#ifndef __QUADTREE_H
#define __QUADTREE_H


/**
 * @file quadtree.h
 *  
 * Description of the program objectives.
 * All necessary references.
 */

#include <stdio.h> 

typedef struct 
{
  struct quadtree *noeud;
  
}pQuadtree;

struct quadtree
{
  pQuadtree sons[4];
  double M0,M1[3],M2[3];
};

extern pQuadtree create_quadtree();
extern void quadtree_subdivide(pQuadtree);

#endif
